/* Header file for the EEPROM read and write functions
    Author:
    Version:
    Date:
*/

#ifndef EEPROM_H 
#define EEPROM_H

// EEPROM memory for non volatile variables
// Each variable is defined by its first and last position
#define EEPROM_SIZE 128

// Configuración WiFi / Carga de batería 
// SSID of station. 16 character string + terminating ASCII null
const unsigned int EEPROM_AP_SSID_CONF[] = {0, 15}; 
// Password of station. 16 character string + terminating ASCII null
const unsigned int EEPROM_AP_PASSWORD_CONF[] = {16, 31};
// Según las transparencias, no es necesario añadir los parámetros de la parte bluetooth.

// Puente MavLink
// udp_gcs_port_conf (1455) unsigned int (2 bytes) 
const unsigned int EEPROM_MAVLINK_UDP_GCS_PORT_CONF[] = {32, 33};
// ssid_conf (SEU-MAVLINK) 16 character string + terminating ASCII null
const unsigned int EEPROM_MAVLINK_SSID_CONF[] = {34, 49};
// password_conf (12345678) 16 character string + terminating ASCII null
const unsigned int EEPROM_MAVLINK_PASSWORD_CONF[] = {50, 65};

// Control remoto WiFi con salida PPM
// udp_rc_port_conf (42724) unsigned int (2 bytes) 
const unsigned int EEPROM_RC_UDP_PORT_CONF[] = {66, 67};
// ssid_conf (SEU-RC) 
const unsigned int EEPROM_RC_SSID_CONF[] = {68, 83};
// password_conf (12345678)
const unsigned int EEPROM_RC_PASSWORD_CONF[] = {84, 99};

bool eeprom_init();
bool eeprom_write(const unsigned int key[], String keyval);
bool eeprom_write(const unsigned int key[], uint16_t keyval);
bool eeprom_write_as_int(const unsigned int key[], String keyval);
String eeprom_read_string(const unsigned int key[]);
uint16_t eeprom_read_int(const unsigned int key[]);
String eeprom_read_int_as_string(const unsigned int key[]);
void eeprom_commit();

#endif