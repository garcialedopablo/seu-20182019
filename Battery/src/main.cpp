/* Example of BLE advertising using iBeacons. It is based on the C++ BLE library

  iBeacons are an example of application on BLE advertising frames.
  The payload of advertising BLE frames is made up of Advertising Data (AD) elements,
  with a total maximum payload of 31 bytes for BLE 4.2. 
  Each AD element is formatted as follows: 
   - Length of AD element: 1 byte (excluding itself)
   - Advertising Type: 1 byte. 
     See https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
   - Data

  An iBeacon advertising frame has two AD elements:
  * First AD element for flags. Defines the capabilities of the device and it is a mandatory
    requirement in the BLE specification
    - Byte 0: Length AD element = 0x02
    - Byte 1: Advertising Type = 0x01 => Flags
    - Byte 2: Value of flags (for example 0x04 indicates that BR/EDR is not supported) 
  * Second AD element for iBeacon data.
    - Byte 0: Length of AD element: 1 byte  = 26
    - Byte 1: Advertising Type = 0xFF => Manufacturer Specific Data
    - Bytes 5 to 6: Manufacturer ID = 0x4C00 => Identifies Apple
    - Byte 7: Subtype = 0x02 => Identifies an iBeacon
    - Byte 8: Subtype Length = 21 => Length of the iBeacon data (bytes 9 to 29)
    - Bytes 9-24: Proximity UUID => 16 bytes identifying the iBeacon propietary organization
    - Bytes 25 to 26: Major => Identify an organization geographycal area
    - Bytes 28 to 29: Minor => Identify a location in the previous area
    - Byte 30: Signal power => Power in dBs of the transmitted signal at one meter. 
      Together with the RSSI at the receiver can be used to estimate the distance.
  
  Remarks. The scanning device after detecting an advertising frame may request a 
  second advertising frame to the broadcaster. The ESP32 answers with a new advertising 
  frame containing additional ADs: 
  * Flags. This AD is mandatory in all the advertising frames.
  * Complete Local Name AD. The name of the Bluetooth device.
  * Tx Power Level. The transmitting power in dBs.
  * Slave Connection Interval Range in ms. Accepted intervals when working as a slave. 

    Author: Jose Maria Lopez 
    Version: 1.0
    Date: December 2018
*/

#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEBeacon.h>
#include <debug.h>
// Period and advertising window
#define PERIOD_MS 5000 // Deep sleep time in ms between advertising windows
#define ADVERTISING_MS 400 // Advertising window in ms

// UUID generated with https://www.uuidgenerator.net/
// You can choose one to identify your organization
#define UNIOVI_UUID  "d2c495de-3211-4be2-85ef-e494a62cf853" 

// Define a physical area of your organization (major) and a place inside (minor) 
#define EPI_GIJON_MAJOR 0x1234 // 4660 in decimal

BLEAdvertising *pAdvertising;
RTC_DATA_ATTR float battery_level = 100;

void set_advertising_frame();

void update_battery_level() {

  if (battery_level > 0) {
    battery_level--;
  }
  
  DEBUG_MSG("Nivel de batería actual: %f\n", battery_level);
  set_advertising_frame();
}

///////////////////////////////////////////////////////////////////////////////
// Fill the advertising frame with an iBeacon
///////////////////////////////////////////////////////////////////////////////
void set_advertising_frame() {

  // Create an object to store the data (payload) of the advertising frame
  // It includes the first Advertisment Data element with the flags
  BLEAdvertisementData advertisementData = BLEAdvertisementData();
  advertisementData.setFlags(0x04); // Flags = 0x04 => BR/EDR not supported

  // Create an iBeacon object
  BLEBeacon iBeacon = BLEBeacon();

  // Fill the iBeacon fields 
  iBeacon.setManufacturerId(0x4C00); // Identifies the manufacturer as Apple
  iBeacon.setProximityUUID(BLEUUID(UNIOVI_UUID)); // Identifies your organization
  iBeacon.setMajor(EPI_GIJON_MAJOR); // Identifies a broad area in the organization
  iBeacon.setSignalPower(-60); // RSSI at 1 meter
  
  // This field usually identifies a place in the prevous area.
  // However, we will violate the protocol and use it to transmit the current battery level.
  iBeacon.setMinor(battery_level); 

  // Set the iBeacon AD element
  std::string iBeaconData = "";  
  iBeaconData += (char)26;     // Length field
  iBeaconData += (char)0xFF;   // Advertising type. Value 0xFF for manufacturer specific
  iBeaconData += iBeacon.getData(); // Data comes from iBeacon fields 

  // Add the iBeacon AD element. Now advertisementData contains booth AD elements
  advertisementData.addData(iBeaconData);

  // Set the advertisement data of the frame  
  pAdvertising->setAdvertisementData(advertisementData);
}

void setup() {    

  Serial.begin(115200);
  Serial.println("\n\niBeacon device wakes up");

  // Initialize the BLE device
  BLEDevice::init("UniOvi-EPIG-Lab3");

  // Create a BLE server
  BLEServer *pServer = BLEDevice::createServer();

  // Get the advertising object from the server
  pAdvertising = pServer->getAdvertising();
  
  // Fill the BLE advertising frame with iBeacon data 
  set_advertising_frame();
}

void loop() 
{
  update_battery_level();

  // Advertise for ADVERTISING_MS miliseconds
  pAdvertising->start();
  delay(ADVERTISING_MS);

  // Stop advertising
  pAdvertising->stop();
 
  // Enter deep sleep mode for PEROOD_MS microseconds
  Serial.println("\niBeacon device sleeps zzZZZzz");
  Serial.flush();
  esp_sleep_enable_timer_wakeup(PERIOD_MS*1000);
  esp_deep_sleep_start();
}
