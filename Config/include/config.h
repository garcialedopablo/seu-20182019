/* Header file for the device configuration phase manager
    Author: Pablo García Ledo
    Version: 0.1
    Date: 2019.02.20
*/

#ifndef CONFIG_H
#define CONFIG_H

// Punto de entrada de la etapa de configuración del programa
//
// Permite la pulsación del botón de la placa, la cual causará el
// inicio de un servidor web de configuración.
void preempt_config();

#endif