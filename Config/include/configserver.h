/* Header file for the device configuration phase manager
    Author: Pablo García Ledo
    Version: 0.1
    Date: 2019.02.20
*/

#ifndef CONFIG_SERVER_H
#define CONFIG_SERVER_H

extern int client_connected;
extern int configuration_complete;

void start_server();
void update_server();
void stop_server();

#endif