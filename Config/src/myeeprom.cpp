/* EEPROM read and write functions
    Author: 
    Version:
    Date:
*/

#include <EEPROM.h>
#include "myeeprom.h"

///////////////////////////////////////////////////////////////////////////////
// Init the EEPROM
///////////////////////////////////////////////////////////////////////////////
bool eeprom_init()
{
  if (!EEPROM.begin(EEPROM_SIZE)) {
    return false;
  }

  // Confifuracion Wifi
  if(eeprom_read_string(EEPROM_AP_SSID_CONF).length() == 0)
    eeprom_write(EEPROM_AP_SSID_CONF, "SEU-AP");  

  if(eeprom_read_string(EEPROM_AP_PASSWORD_CONF).length() == 0)
    eeprom_write(EEPROM_AP_PASSWORD_CONF, "12345678");

  // Puente MavLink
  if(eeprom_read_int(EEPROM_MAVLINK_UDP_GCS_PORT_CONF) == 0)
    eeprom_write(EEPROM_MAVLINK_UDP_GCS_PORT_CONF, 1455);

  if(eeprom_read_string(EEPROM_MAVLINK_SSID_CONF).length() == 0)
    eeprom_write(EEPROM_MAVLINK_SSID_CONF, "SEU-MAVLINK");  

  if(eeprom_read_string(EEPROM_MAVLINK_PASSWORD_CONF).length() == 0)
    eeprom_write(EEPROM_MAVLINK_PASSWORD_CONF, "12345678");

  // Control remoto WiFi con salida PPM

  if(eeprom_read_int(EEPROM_RC_UDP_PORT_CONF) == 0)
    eeprom_write(EEPROM_RC_UDP_PORT_CONF, 42724);

  if(eeprom_read_string(EEPROM_RC_SSID_CONF).length() == 0)
    eeprom_write(EEPROM_RC_SSID_CONF, "SEU-RC");  

  if(eeprom_read_string(EEPROM_RC_PASSWORD_CONF).length() == 0)
    eeprom_write(EEPROM_RC_PASSWORD_CONF, "12345678");

  // Una vez iniciada la eeprom y las variables
  // se intentan guardar los cambios y se notifica el resultado
  return EEPROM.commit();
}

///////////////////////////////////////////////////////////////////////////////
// Commit the write operations
///////////////////////////////////////////////////////////////////////////////
void eeprom_commit()
{
  EEPROM.commit();
}

///////////////////////////////////////////////////////////////////////////////
// Write a key value in EEPROM. Key values are strings
//  key[0] => First byte position of the key in EEPROM 
//  key[1] => Last byte position of the key in EEPROM 
// The value is a null terminated string. All the characters just after 
// the null terminator until the size of the key must be filled with 0xAA 
// to identify a correct key.
// Returns true if the key value fits into the key reserved space and false 
// otherwise.
///////////////////////////////////////////////////////////////////////////////
bool eeprom_write(const unsigned int key[], String keyval)
{
  int i; // Index in the keyval string

  // Return false if the keyval does not fit 
  if (keyval.length() > key[1] - key[0])
    return false;
  
  // Write each character in the string including the ASCII null
  for (i = 0; i < keyval.length(); i++)
    EEPROM.write(key[0] + i, keyval.charAt(i));
  EEPROM.write(key[0] + i, '\0');

  // Fill the remaining bytes with the value 0xAA
  i++;
  while (key[0] + i <= key[1])
  {
    EEPROM.write(key[0] + i, 0xAA);
    i++;
  }

  return true;
}

// Returns false if there's not enough space for an int
// in the given key or if the string can't be parsed into an int.
// Otherwise, writes the value and returns true. 
bool eeprom_write_as_int(const unsigned int key[], String keyval)
{
  int i;

  i = keyval.toInt();
  
  // Artificially avoid parsing check in order to allow
  // saving the 0 value, which will trigger the usage of
  // default values on EEPROM init.
  /*
  if (i == 0) // toInt return 0 when the string is not a int
  {
    return false;
  }
  */

  return eeprom_write(key, i);
}

// Returns false if there's not enough space for an int in the given key.
// Otherwise, writes the value and returns true.
bool eeprom_write(const unsigned int key[], uint16_t keyval) 
{
  if (key[1] - key[0] < 1) 
  {
    return false;
  }

  EEPROM.writeUShort(key[0], keyval);
  return true;
}

///////////////////////////////////////////////////////////////////////////////
// Read the value stored in EEPROM for a key. Each key has two components:
//  key[0] => First byte position of the key in EEPROM 
//  key[1] => Last byte position of the key in EEPROM 
// The value is a null terminated string. All the characters just after the null 
// terminator until the size of the key must be filled with 0xAA to identify a 
// correct key.
// Returns the key value, or an empty string if the key is not valid .
///////////////////////////////////////////////////////////////////////////////
String eeprom_read_string(const unsigned int key[])
{
  String keyval; 
  bool keyend = false; // The key value has reached its end?
  char string[2] ; 

  // For each byte stored in the EEPROM reserved space for the key
  for (int i = key[0]; i <= key[1]; i++)
  {
    sprintf(string, "%c", EEPROM.read(i)); // Read the byte from EEPROM
    if (keyend && string[0] != 0xAA) // The remainder bytes of the key must be filled up with 0xAA
      return "";
    if (!keyend && (string[0] != '\0'))  // Add character to key value wntil the end of the string
      keyval += string[0];
    if (string[0] == '\0') // Detect the end of the string
      keyend = true;
  }
  if (!keyend)
    return "";
  return keyval;
}

String eeprom_read_int_as_string(const unsigned int key[]) 
{
  int value;
  String as_string;

  value = EEPROM.readUShort(key[0]);

  as_string = String(value);
  return as_string;
}

uint16_t eeprom_read_int(const unsigned int key[]) 
{
  return EEPROM.readUShort(key[0]);
}