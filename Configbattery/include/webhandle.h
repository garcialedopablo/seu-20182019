/* Header file for the web server handles
    Author:
    Version:
    Date:
*/

#ifndef WEBHANDLE_H 
#define WEBHANDLE

void handleRoot();
void handleNotFound();

#endif