#include <Arduino.h>
#include "config.h"
#include "configserver.h"
#include "debug.h"
#include "esp32-hal-gpio.h"

// Nota - Conf. pullup
// Probablemente no necesitemos tener esto en cuenta, pero:
// El circuito proporcionado utiliza un pulsador pullup (voltaje positivo por defecto)
// Los pines 34-39 no permiten configuración pullup en un chip ESP32
// Es por esto que de necesitar configurar el pin como pullup, tendríamos que cambiar de pin
#define PIN_PULSADOR 34 
#define PIN_LED 26

// Controla el parpadeo del led
//
// blink_duration = cantidad de milisegundos a esperar encendido
// t = cantidad de milisegundos desde el comienzo del parpadeo
void update_blink(unsigned long blink_duration, unsigned long t);

// Lee el estado actual del pulsador
bool get_input();

// Espera unos segundos a una pulsación y devuelve false si no ocurre
// incluye parpadeo de led
bool wait_for_input();

// Entra al modo de configuración, arranca el servidor web y espera
// conexión. Incluye parpadeo de led
void enter_config_mode();


// ------- Implementación

void preempt_config() {
    
    // Configurar puertos necesarios
    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_PULSADOR, INPUT);

    // Esperar a que se pulse el botón
    // Parpadeo lento durante 5 segundos
    DEBUG_MSG("Esperando pulsación para entrar a modo de configuración.\n");
    if (wait_for_input()) 
    {
        // Si se pulsa el botón, entrar en el modo de configuración
        // Parpadeo rápido durante 60 segundos
        DEBUG_MSG("Pulsación detectada. Entrando en modo de configuración.\n");
        enter_config_mode();
        DEBUG_MSG("Saliendo del modo de configuración.\n");
    }
    else 
    {
        DEBUG_MSG("Tiempo de espera agotado. Omitiendo modo de configuración.\n");
    }
    
    // Al terminar el modo de configuración, asegurarse de que 
    // el led queda apagado
    digitalWrite(PIN_LED, LOW);

    // Terminar y escapar a loop()
}

bool wait_for_input() {

    // Parpadeo lento 
    // Esperar por pulsación de botón durante 5 segundos

    unsigned long initTime;
    unsigned long waitTime;

    initTime = millis();
    waitTime = initTime + 5000;

    while (millis() < waitTime) {
        
        // Comprobar estado de boton
        if (get_input()) {
            return true;
        }

        // Actualizar led
        update_blink(500, millis() - initTime);
    }

    return false;
}

void enter_config_mode() {

    // parpadeo rápido 60 segundos, si no se conecta un cliente, salir.

    unsigned long initTime;
    unsigned long waitTime;

    DEBUG_MSG("Iniciando servidor.\n");
    start_server();

    initTime = millis();
    waitTime = initTime + 60000;

    DEBUG_MSG("Esperando conexión de cliente durante %u ms.\n", waitTime);
    while (!client_connected && millis() < waitTime) {
        update_server();
        update_blink(200, millis() - initTime);
    }
    
    if (client_connected) {
        DEBUG_MSG("Conexión de cliente registrada. Esperando confirmación de configuración.\n");
    }
    else 
    {
        DEBUG_MSG("Ha finalizado el tiempo de espera a conexión de cliente.\n");
    }

    while (client_connected && !configuration_complete) {
        update_server();
        digitalWrite(PIN_LED, HIGH);
    }

    if (configuration_complete) 
    {
        DEBUG_MSG("Confirmación de configuración recibida.\n");
    }

    DEBUG_MSG("Cerrando servidor web.\n");
    stop_server();
}


void update_blink(unsigned long blink_duration, unsigned long t) {

    unsigned int n;
    uint8_t encender;

    // Enciende el led si han pasado un numero impar de  
    // duraciones de parpadeo.
    n = t / blink_duration;
    encender = n % 2;

    digitalWrite(PIN_LED, encender);
}

// Devuelve el valor actual del pulsador
bool get_input() {

    // Asume que el pin ha sido configurado, y que un valor
    // low corresponde con un pulsador pulsado
    
    int val;

    val = digitalRead(PIN_PULSADOR);

    if (val == LOW) {
        return true;
    }
    else {
        return false;
    }
}
