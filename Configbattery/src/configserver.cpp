#include <Arduino.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include "webhandle.h"
#include "myeeprom.h"
#include "debug.h"

// SSID and password for the AP mode
char ssid_conf[]     = "DRONE-CONFIG"; 
char password_conf[] = "12345678"; 

// Web server on TCP port 80
WebServer server(80);

int client_connected = 0;
int configuration_complete = 0;

void run_ap_webserver(char ssid[], char password[]);
void update_server();

void start_server() {

    // ssid_conf y password for connecting as a WiFi station
    String ssid_sta;
    String password_sta;
    
    // Initialize the EEPROM
    if (!eeprom_init()) {
        DEBUG_MSG("Failed initializing EEPROM\n");
    }

    // Read the connection parameters from EEPROM
    ssid_sta = eeprom_read_string(EEPROM_AP_SSID_CONF);
    password_sta = eeprom_read_string(EEPROM_AP_PASSWORD_CONF);

    // Connection parameters to char arrays
    char ssid_array[33];
    char password_array[33];
    ssid_sta.toCharArray(ssid_array, sizeof(ssid_array));
    password_sta.toCharArray(password_array, sizeof(password_array));

    DEBUG_MSG("Stored ssid_conf: %s\tStored password: %s\n", 
            ssid_array, password_array);

    // If the connection parameters in EEPROM are valid
    if (ssid_sta.length() > 0 || password_sta.length() > 0)
    {
        run_ap_webserver(ssid_array, password_array);
    }
    else
    {
        DEBUG_MSG("Valores guardados no válidos. Usando configuración por defecto.\n");
        DEBUG_MSG("Default ssid_conf: %s\tDefault password: %s\n", 
                ssid_conf, password_conf);
        run_ap_webserver(ssid_conf, password_conf);
    }
}

///////////////////////////////////////////////////////////////////////////////
// Run as acess point with a web server for configuration
///////////////////////////////////////////////////////////////////////////////
void run_ap_webserver(char ssid[], char password[]) {

    // Set the access point
    DEBUG_MSG("Setting AP with ssid_conf: %s", ssid);
    DEBUG_MSG(WiFi.softAP(ssid, password) ? "  -> OK" : "  ->Failed");

    // Show the IP address
    char ip_address[16];
    WiFi.softAPIP().toString().toCharArray(ip_address, sizeof(ip_address));
    DEBUG_MSG("  IP address: %s\n", ip_address);
    
    // Start the web server
    server.on("/index.html", handleRoot);
    server.onNotFound(handleNotFound);
    server.begin();
    DEBUG_MSG("Web server started.\n");
}

void update_server() {
    server.handleClient();
}

void stop_server() {
    server.close();
}