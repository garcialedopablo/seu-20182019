/* Wifi configuration and BLE battery status
    Author: 
    Version:
    Date:
*/

#include <Arduino.h>
#include "debug.h"
#include "config.h"

// Beaon period and advertising window
#define PERIOD_MS 10000 // Deep sleep time in ms between advertising windows
#define ADVERTISING_MS 100 // Advertising window in ms

///////////////////////////////////////////////////////////////////////////////
// Arduino setup fuction
///////////////////////////////////////////////////////////////////////////////
void setup()
{
  // Set baud rate for the debug port
#ifdef DEBUG_ESP_PORT
  Serial.begin(DEBUG_BAUD_RATE);
  DEBUG_MSG("\n\n");
#endif

  // Comienza etapa de configuración
  preempt_config();
}

///////////////////////////////////////////////////////////////////////////////
// Arduino loop fuction
///////////////////////////////////////////////////////////////////////////////
void loop() {}