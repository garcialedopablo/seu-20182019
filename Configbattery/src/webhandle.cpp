/* Web server handles using library WebSever
    Author: 
    Version:
    Date:
*/

#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include "myeeprom.h"
#include "configserver.h"
#include "debug.h"

// Variale server is defined in other source file
extern WebServer server;

// Simplifies treatment of POST requests
struct arg_handler {
    String name;
    const unsigned int key[2];
    String (*read)(const unsigned int[]);
    bool (*write)(const unsigned int[], String);
};

const arg_handler arg_handlers[8] = {
    {"ap_ssid", EEPROM_AP_SSID_CONF[0], EEPROM_AP_SSID_CONF[1], eeprom_read_string, eeprom_write},
    {"ap_paswd", EEPROM_AP_PASSWORD_CONF[0], EEPROM_AP_PASSWORD_CONF[1], eeprom_read_string, eeprom_write},
    {"mavlink_upd_port", EEPROM_MAVLINK_UDP_GCS_PORT_CONF[0], EEPROM_MAVLINK_UDP_GCS_PORT_CONF[1], eeprom_read_int_as_string, eeprom_write_as_int},
    {"mavlink_ssid", EEPROM_MAVLINK_SSID_CONF[0], EEPROM_MAVLINK_SSID_CONF[1], eeprom_read_string, eeprom_write},
    {"mavlink_paswd", EEPROM_MAVLINK_PASSWORD_CONF[0], EEPROM_MAVLINK_PASSWORD_CONF[1], eeprom_read_string, eeprom_write},
    {"rc_upd_port", EEPROM_RC_UDP_PORT_CONF[0], EEPROM_RC_UDP_PORT_CONF[1], eeprom_read_int_as_string, eeprom_write_as_int},
    {"rc_ssid", EEPROM_RC_SSID_CONF[0], EEPROM_RC_SSID_CONF[1], eeprom_read_string, eeprom_write},
    {"rc_paswd", EEPROM_RC_PASSWORD_CONF[0], EEPROM_RC_PASSWORD_CONF[1], eeprom_read_string, eeprom_write}
};

const unsigned int num_arg_handlers = (sizeof(arg_handlers)/sizeof(arg_handler));

// Returns true if the new value was different from the previous one
bool save(arg_handler handler, String value);

///////////////////////////////////////////////////////////////////////////////
// Handle the server response for /index.html resource
///////////////////////////////////////////////////////////////////////////////
void handleRoot() 
{   
    bool was_post = false;
    bool was_success = false;

    client_connected = 1;

    // Get the value of ssid and password arguments
    for (int i = 0; i < server.args(); i++) 
    {
        DEBUG_MSG("Procesando argumento %i.\n", i);
        
        // Match arguments against existing eeprom keys
        for (int j = 0; j < num_arg_handlers; j++) 
        {
            if (server.argName(i).equals(arg_handlers[j].name)){
                // If some name matches, assume POST
                was_post = true;
                was_success |= save(arg_handlers[j], server.arg(i));
            }
        }
    }

    // If there was a POST request that made changes without errors, consider configuration complete
    if (was_post & was_success) 
    {
        DEBUG_MSG("Se ha procesado una petición POST con configuración correcta.\n");

        // Register configuration complete
        eeprom_commit();
        configuration_complete = 1;

        // Answer that the device has been configured
        server.send(
            200, 
            "text/html", 
            #include "wwwroot/index-configurado.html.include"
            );
    }
    // Otherwise, (be it GET or failed POST) serve a configuration form
    else 
    {
        if (was_post && !was_success) 
        {
            DEBUG_MSG("Se ha encontrado un error procesando la petición POST.\n");
        }
        DEBUG_MSG("Enviando página index.\n");

        char buffer[3600];
        snprintf(
            buffer, 
            sizeof(buffer),
            #include "wwwroot/index.html.include"
            , 
            eeprom_read_string(EEPROM_AP_SSID_CONF).c_str(),
            eeprom_read_string(EEPROM_AP_PASSWORD_CONF).c_str(),
            eeprom_read_int(EEPROM_MAVLINK_UDP_GCS_PORT_CONF),
            eeprom_read_string(EEPROM_MAVLINK_SSID_CONF).c_str(),
            eeprom_read_string(EEPROM_MAVLINK_PASSWORD_CONF).c_str(),
            eeprom_read_int(EEPROM_RC_UDP_PORT_CONF),
            eeprom_read_string(EEPROM_RC_SSID_CONF).c_str(),
            eeprom_read_string(EEPROM_RC_PASSWORD_CONF).c_str()
        );

        server.send(
            200,
            "text/html",
            (String) buffer
        );
    }
}

///////////////////////////////////////////////////////////////////////////////
// Handle the server response for a not found resource
///////////////////////////////////////////////////////////////////////////////
void handleNotFound() 
{
    client_connected = 1;

    String message = "File Not Found\n\n";
    server.send(404, "text/plain", message);
}

bool save(arg_handler handler, String value) {

    String previous_value;
    bool result;
    
    DEBUG_MSG(
        "Identificada clave de configuración en petición POST; %s:%s\n",
        handler.name.c_str(),
        value.c_str()
    );
    
    result = handler.write(handler.key, value);

    DEBUG_MSG(
        "Resultado del guardado: %d\n\n", 
        result
    );

    return result;
}